EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Battery_Management:LTC4054XES5-4.2 U?
U 1 1 6190180B
P 3800 2300
AR Path="/6190180B" Ref="U?"  Part="1" 
AR Path="/618FB971/6190180B" Ref="U4"  Part="1" 
F 0 "U4" H 4050 2850 50  0000 L CNN
F 1 "LTC4054XES5-4.2" H 4050 2750 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 3800 1800 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/405442xf.pdf" H 3800 2200 50  0001 C CNN
	1    3800 2300
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LD3985G33R_TSOT23 U5
U 1 1 61902DEA
P 3750 5200
F 0 "U5" H 3750 5542 50  0000 C CNN
F 1 "LD3985G33R_TSOT23" H 3750 5451 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 3750 5525 50  0001 C CIN
F 3 "http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/CD00003395.pdf" H 3750 5250 50  0001 C CNN
	1    3750 5200
	1    0    0    -1  
$EndComp
$Comp
L benou-mechanical:LiPo_20*30*4 U6
U 1 1 61904DCE
P 5800 2300
F 0 "U6" H 5800 2300 50  0001 C CNN
F 1 "LiPo_20*30*4" H 5850 2200 50  0000 L CNN
F 2 "benou-footprints:LiPo_20x30x4" H 5800 2300 50  0001 C CNN
F 3 "" H 5800 2300 50  0001 C CNN
	1    5800 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 6193F0A7
P 4350 2150
AR Path="/6193F0A7" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/6193F0A7" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 4350 2000 50  0001 C CNN
F 1 "+BATT" H 4365 2323 50  0000 C CNN
F 2 "" H 4350 2150 50  0001 C CNN
F 3 "" H 4350 2150 50  0001 C CNN
	1    4350 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 619A7633
P 3350 1800
AR Path="/619A7633" Ref="C?"  Part="1" 
AR Path="/618FB971/619A7633" Ref="C13"  Part="1" 
F 0 "C13" H 3442 1846 50  0000 L CNN
F 1 "1uF" H 3442 1755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 1800 50  0001 C CNN
F 3 "~" H 3350 1800 50  0001 C CNN
	1    3350 1800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 619AA12B
P 3350 1700
AR Path="/619AA12B" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619AA12B" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 3350 1550 50  0001 C CNN
F 1 "+5V" H 3365 1873 50  0000 C CNN
F 2 "" H 3350 1700 50  0001 C CNN
F 3 "" H 3350 1700 50  0001 C CNN
	1    3350 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1700 3800 1700
Wire Wire Line
	3800 1700 3800 2000
Connection ~ 3350 1700
$Comp
L power:GND #PWR?
U 1 1 619ACFF4
P 3800 2700
AR Path="/619ACFF4" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619ACFF4" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 3800 2450 50  0001 C CNN
F 1 "GND" H 3805 2527 50  0000 C CNN
F 2 "" H 3800 2700 50  0001 C CNN
F 3 "" H 3800 2700 50  0001 C CNN
	1    3800 2700
	1    0    0    -1  
$EndComp
Text Notes 1600 2400 0    50   ~ 0
I_bat=(V_prog/R_prog)*1000
Text Notes 1600 2500 0    50   ~ 0
V_prog=1V   (in CC mode)\n
$Comp
L Device:R_Small R?
U 1 1 619AF56D
P 3150 2600
AR Path="/618FBB16/619AF56D" Ref="R?"  Part="1" 
AR Path="/618FB971/619AF56D" Ref="R2"  Part="1" 
F 0 "R2" H 3209 2646 50  0000 L CNN
F 1 "2.7K" H 3209 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 3150 2600 50  0001 C CNN
F 3 "~" H 3150 2600 50  0001 C CNN
	1    3150 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2500 3150 2400
Wire Wire Line
	3150 2400 3400 2400
$Comp
L power:GND #PWR?
U 1 1 619AF782
P 3150 2700
AR Path="/619AF782" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619AF782" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 3150 2450 50  0001 C CNN
F 1 "GND" H 3155 2527 50  0000 C CNN
F 2 "" H 3150 2700 50  0001 C CNN
F 3 "" H 3150 2700 50  0001 C CNN
	1    3150 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 619AFA1C
P 2850 2600
AR Path="/618FBB16/619AFA1C" Ref="R?"  Part="1" 
AR Path="/618FB971/619AFA1C" Ref="R1"  Part="1" 
F 0 "R1" H 2909 2646 50  0000 L CNN
F 1 "NP" H 2909 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2850 2600 50  0001 C CNN
F 3 "~" H 2850 2600 50  0001 C CNN
	1    2850 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2500 2850 2400
Wire Wire Line
	2850 2400 3150 2400
Connection ~ 3150 2400
$Comp
L power:GND #PWR?
U 1 1 619B007E
P 2850 2700
AR Path="/619B007E" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B007E" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 2850 2450 50  0001 C CNN
F 1 "GND" H 2855 2527 50  0000 C CNN
F 2 "" H 2850 2700 50  0001 C CNN
F 3 "" H 2850 2700 50  0001 C CNN
	1    2850 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 619B0412
P 3350 1900
AR Path="/619B0412" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B0412" Ref="#PWR0130"  Part="1" 
F 0 "#PWR0130" H 3350 1650 50  0001 C CNN
F 1 "GND" H 3355 1727 50  0000 C CNN
F 2 "" H 3350 1900 50  0001 C CNN
F 3 "" H 3350 1900 50  0001 C CNN
	1    3350 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2300 4350 2300
Wire Wire Line
	4350 2300 4350 2150
$Comp
L Connector_Generic:Conn_01x01 J?
U 1 1 619B1F83
P 5400 2300
AR Path="/619B1F83" Ref="J?"  Part="1" 
AR Path="/618FB971/619B1F83" Ref="J9"  Part="1" 
F 0 "J9" H 5318 2167 50  0000 C CNN
F 1 "Conn_01x01" H 5318 2166 50  0001 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5400 2300 50  0001 C CNN
F 3 "~" H 5400 2300 50  0001 C CNN
	1    5400 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J?
U 1 1 619B1F89
P 5400 2550
AR Path="/619B1F89" Ref="J?"  Part="1" 
AR Path="/618FB971/619B1F89" Ref="J10"  Part="1" 
F 0 "J10" H 5318 2417 50  0000 C CNN
F 1 "Conn_01x01" H 5318 2416 50  0001 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5400 2550 50  0001 C CNN
F 3 "~" H 5400 2550 50  0001 C CNN
	1    5400 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 619B2103
P 5200 2300
AR Path="/619B2103" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B2103" Ref="#PWR0131"  Part="1" 
F 0 "#PWR0131" H 5200 2150 50  0001 C CNN
F 1 "+BATT" H 5215 2473 50  0000 C CNN
F 2 "" H 5200 2300 50  0001 C CNN
F 3 "" H 5200 2300 50  0001 C CNN
	1    5200 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 619B250D
P 5200 2550
AR Path="/619B250D" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B250D" Ref="#PWR0132"  Part="1" 
F 0 "#PWR0132" H 5200 2300 50  0001 C CNN
F 1 "GND" H 5205 2377 50  0000 C CNN
F 2 "" H 5200 2550 50  0001 C CNN
F 3 "" H 5200 2550 50  0001 C CNN
	1    5200 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 619B2C06
P 2900 5200
AR Path="/619B2C06" Ref="C?"  Part="1" 
AR Path="/618FB971/619B2C06" Ref="C12"  Part="1" 
F 0 "C12" H 2992 5246 50  0000 L CNN
F 1 "1uF" H 2992 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2900 5200 50  0001 C CNN
F 3 "~" H 2900 5200 50  0001 C CNN
	1    2900 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 619B3695
P 4500 5200
AR Path="/619B3695" Ref="C?"  Part="1" 
AR Path="/618FB971/619B3695" Ref="C15"  Part="1" 
F 0 "C15" H 4592 5246 50  0000 L CNN
F 1 "1uF" H 4592 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4500 5200 50  0001 C CNN
F 3 "~" H 4500 5200 50  0001 C CNN
	1    4500 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 619B5B7A
P 4050 5300
AR Path="/619B5B7A" Ref="C?"  Part="1" 
AR Path="/618FB7C9/619B5B7A" Ref="C?"  Part="1" 
AR Path="/618FB971/619B5B7A" Ref="C14"  Part="1" 
F 0 "C14" H 4142 5346 50  0000 L CNN
F 1 "10nF" H 4142 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4050 5300 50  0001 C CNN
F 3 "~" H 4050 5300 50  0001 C CNN
	1    4050 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 619B5D56
P 3750 5500
AR Path="/619B5D56" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B5D56" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 3750 5250 50  0001 C CNN
F 1 "GND" H 3755 5327 50  0000 C CNN
F 2 "" H 3750 5500 50  0001 C CNN
F 3 "" H 3750 5500 50  0001 C CNN
	1    3750 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 619B6131
P 4050 5400
AR Path="/619B6131" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B6131" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 4050 5150 50  0001 C CNN
F 1 "GND" H 4055 5227 50  0000 C CNN
F 2 "" H 4050 5400 50  0001 C CNN
F 3 "" H 4050 5400 50  0001 C CNN
	1    4050 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 619B6501
P 4500 5300
AR Path="/619B6501" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B6501" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 4500 5050 50  0001 C CNN
F 1 "GND" H 4505 5127 50  0000 C CNN
F 2 "" H 4500 5300 50  0001 C CNN
F 3 "" H 4500 5300 50  0001 C CNN
	1    4500 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 619B669C
P 2900 5300
AR Path="/619B669C" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B669C" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 2900 5050 50  0001 C CNN
F 1 "GND" H 2905 5127 50  0000 C CNN
F 2 "" H 2900 5300 50  0001 C CNN
F 3 "" H 2900 5300 50  0001 C CNN
	1    2900 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 5100 3250 5100
Wire Wire Line
	4050 5100 4500 5100
$Comp
L power:VCC #PWR?
U 1 1 619B9BC0
P 4500 5100
AR Path="/619B9BC0" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B9BC0" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 4500 4950 50  0001 C CNN
F 1 "VCC" H 4515 5273 50  0000 C CNN
F 2 "" H 4500 5100 50  0001 C CNN
F 3 "" H 4500 5100 50  0001 C CNN
	1    4500 5100
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 619B9DDD
P 2900 5100
AR Path="/619B9DDD" Ref="#PWR?"  Part="1" 
AR Path="/618FB971/619B9DDD" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 2900 4950 50  0001 C CNN
F 1 "+BATT" H 2915 5273 50  0000 C CNN
F 2 "" H 2900 5100 50  0001 C CNN
F 3 "" H 2900 5100 50  0001 C CNN
	1    2900 5100
	1    0    0    -1  
$EndComp
Connection ~ 2900 5100
Wire Wire Line
	3250 5100 3250 5200
Wire Wire Line
	3250 5200 3450 5200
Connection ~ 3250 5100
Wire Wire Line
	3250 5100 3450 5100
Text Label 3050 2300 0    50   ~ 0
NCHRG
Wire Wire Line
	3050 2300 3400 2300
$EndSCHEMATC
